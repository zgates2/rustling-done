// threads1.rs
// Make this compile! Execute `rustlings hint threads1` for hints :)
// The idea is the thread spawned on line 22 is completing jobs while the main thread is
// monitoring progress until 10 jobs are completed. Because of the difference between the
// spawned threads' sleep time, and the waiting threads sleep time, when you see 6 lines
// of "waiting..." and the program ends without timing out when running,
// you've got it :)
use std::sync::{Arc, Mutex};
use std::thread;
use std::time::Duration;

struct JobStatus {
    jobs_completed: u32,
}

fn main() {
    let counter = Arc::new(Mutex::new(0));
    let mut handles = vec![];
    for _ in 0..10 {
        let counter = Arc::clone(&counter);
        let handle = thread::spawn(move || {
            thread::sleep(Duration::from_millis(250));
            let mut num = counter.lock().unwrap();
            *num += 1;
        });
        handles.push(handle);
    }
    while *counter.lock().unwrap() < 6 {
        println!("waiting... ");
        println!("Counter was: {}", *counter.lock().unwrap());
        thread::sleep(Duration::from_millis(500));
    }
    println!("Counter was: {}", *counter.lock().unwrap());
}
